package para2;

	import java.applet.*;
	import java.awt.*;
	

	public class EventDriven extends Applet {
		
		Label lblUsername, lblPassword;
		TextField txtUsername, txtPassword;
		Button btnLogin;
		
	    public void init() {
	       setLayout(new GridLayout(3,2));
	       
	       lblUsername = new Label("Username");
	       lblPassword = new Label("Password");
	       txtUsername = new TextField(5);
	       txtPassword = new TextField(5);
	       btnLogin = new Button("Login"); 
	       
	       
	       add(lblUsername);
	       add(txtUsername);
	       add(lblPassword);
	       add(txtPassword);
	       add(btnLogin);
	    }
}
	
	