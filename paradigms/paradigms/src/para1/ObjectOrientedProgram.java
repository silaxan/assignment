package para1;
class MyObjectOrientedProgram {
	
	public void toupercase(String java){
		String index = java.toUpperCase();
	      System.out.println("Uppercase Value :" + index);
	      System.out.println();
	}
	
	public void tolowercase(String java){
		String index = java.toLowerCase();
	      System.out.println("Lowercase Value :" + index);
	      System.out.println();
	}
}

public class ObjectOrientedProgram extends MyObjectOrientedProgram {
	
	public void substring(String java){
		String index = java.substring(5);
	      System.out.println("substring Value :" + index);
	      System.out.println();
	}	
	
	public static void main(String args []) {
		ObjectOrientedProgram demo = new ObjectOrientedProgram();
		String java = "java is good Programming language";
		
		demo.tolowercase(java);
		demo.toupercase(java);
		demo.substring(java);
	}
}




