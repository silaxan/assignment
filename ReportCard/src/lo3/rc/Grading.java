package lo3.rc;

public class Grading {

	public void grading(int marks) {

		if (marks <= 100 & marks >= 75) {
			System.out.println("\t\t  A");
		} else if (marks <= 74 & marks >= 65) {
			System.out.println("\t\t  B");
		} else if (marks <= 64 & marks >= 55) {
			System.out.println("\t\t  C");
		} else if (marks <= 54 & marks >= 35) {
			System.out.println("\t\t  D");
		} else {
			System.out.println("\t\t  F");
		}
	}
}
