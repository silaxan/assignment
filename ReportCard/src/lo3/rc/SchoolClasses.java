package lo3.rc;

import java.util.Scanner;

public class SchoolClasses {
	public static final String school = "J/PUTTALAI M.V";
	static Scanner scan = new Scanner(System.in);

	public void studentoutput(int id, String name, int grade, int maths, int technology, int iT, int tot) {
		Grading demo = new Grading();
		System.out.println();
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		System.out.println();
		System.out.println("NAME\t\t" + "STU_ID\t" + "GRADE\t" + "MATHS\t" + "TECHNOLOGY\t" + "IT\t" + "TOTAL");
		System.out.println();
		System.out
				.println(name + "\t" + id + "\t" + grade + "\t" + maths + "\t" + technology + "\t\t" + iT + "\t" + tot);
		System.out.println();
		System.out.println("SUBJECT\t\tGRADE");
		System.out.println();
		System.out.println("MATHS");
		demo.grading(maths);
		System.out.println("TECHNOLOGY");
		demo.grading(technology);
		System.out.println("IT");
		demo.grading(iT);
		System.out.println();
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		System.out.println();
	}

	public void class9() {
		SchoolClasses demo = new SchoolClasses();
		String[] studentName = { "S.Silaxan", "V.Mathyala", "J.Konsikan", "S.Kannalan", "K.Sayanthan" };
		int[] studentID = { 9001, 9002, 9003, 9004, 9005 };
		int grade = 9;
		int[] maths = { 80, 70, 76, 84, 95 };
		int[] technology = { 90, 77, 79, 74, 95 };
		int[] iT = { 82, 98, 66, 74, 75 };
		System.out.println();
		System.out.println("\t\t\tSCHOOL NAME \t::\t" + school);
		System.out.println();
		for (int i = 0; i < 5; i++) {
			int tot = maths[i] + technology[i] + iT[i];
			demo.studentoutput((studentID[i]), studentName[i], grade, maths[i], technology[i], iT[i], tot);
		}
	}

	public void class10() {
		SchoolClasses demo = new SchoolClasses();
		String[] studentName = { "S.Sriram", "V.Aravinth", "J.Kirushna", "S.Kannan", "K.Santhiran" };
		int[] studentID = { 10001, 10002, 10003, 10004, 10005 };
		int grade = 10;
		int[] maths = { 70, 60, 66, 54, 75 };
		int[] technology = { 50, 57, 99, 88, 75 };
		int[] iT = { 86, 58, 76, 64, 85 };
		System.out.println();
		System.out.println("\t\t\tSCHOOL NAME \t::\t" + school);
		System.out.println();
		for (int i = 0; i < 5; i++) {
			int tot = maths[i] + technology[i] + iT[i];
			demo.studentoutput((studentID[i]), studentName[i], grade, maths[i], technology[i], iT[i], tot);
		}
	}

	public void class11() {
		SchoolClasses demo = new SchoolClasses();
		String[] studentName = { "S.Selvan", "V.Maniyan", "J.Kiruba", "S.Kesavan", "K.Sanjai" };
		int[] studentID = { 11001, 11002, 11003, 11004, 11005 };
		int grade = 11;
		int[] maths = { 60, 90, 86, 88, 85 };
		int[] technology = { 70, 67, 76, 74, 97 };
		int[] iT = { 76, 88, 69, 55, 76 };
		System.out.println();
		System.out.println("\t\t\tSCHOOL NAME \t::\t" + school);
		System.out.println();
		for (int i = 0; i < 5; i++) {
			int tot = maths[i] + technology[i] + iT[i];
			demo.studentoutput((studentID[i]), studentName[i], grade, maths[i], technology[i], iT[i], tot);
		}
	}

	public void addNew() {
		SchoolClasses demo = new SchoolClasses();
		System.out.println("GRADE:");
		int grade = scan.nextInt();
		System.out.println("how many student you will add!!! ");
		int total = scan.nextInt();
		String name[] = new String[total];
		int id[] = new int[total];
		int maths[] = new int[total];
		int technology[] = new int[total];
		int it[] = new int[total];
		int[] tot = new int[total];

		for (int j = 0; j < total; j++) {
			System.out.println("NAME:");
			name[j] = scan.next();

			System.out.println("ID:");
			id[j] = scan.nextInt();

			System.out.println("MATHS:");
			maths[j] = scan.nextInt();

			System.out.println("TECHNOLOGY:");
			technology[j] = scan.nextInt();

			System.out.println("IT:");
			it[j] = scan.nextInt();

			tot[j] = maths[j] + technology[j] + it[j];

		}
		System.out.println();
		System.out.println("\t\t\t YOU CAN SEE RECENTLY ADDED STUDENTS DETAILS!!!\t");
		// System.out.println("\t1. GRADE 9\t2. GRADE 10\t 3. GRADE 11");
		System.out.println("Which Class : ");
		int choice = scan.nextInt();
		if (choice == grade) {
			System.out.println();
			System.out.println("\t\t\tSCHOOL NAME \t::\t" + school);
			System.out.println();
			for (int i = 0; i < 5; i++) {
				demo.studentoutput(id[i], name[i], grade, maths[i], technology[i], it[i], tot[i]);
			}
		} else {
			System.out.println("Details are not availabe");
		}
	}

	public void menupanel() {
		SchoolClasses classes = new SchoolClasses();
		System.out.println();
		System.out.println("\t\t\tWHICH CLASS DO YOU WANT SEE???\t");
		System.out.println("\t\tGRADE-9\t GRADE-10\t GRADE-11");
		System.out.println("Which Class : ");
		int choice = scan.nextInt();

		if (choice == 9) {
			classes.class9();

		} else if (choice == 10) {
			classes.class10();
		} else if (choice == 11) {
			classes.class11();
		}

	}
}
