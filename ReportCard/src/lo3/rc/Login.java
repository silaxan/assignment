package lo3.rc;

import java.util.Scanner;

// This login class is the main console place.
public class Login {
	private int menuChoice;
	private static String username;
	private static String password;
	private static String userinput;
	static Scanner scan = new Scanner(System.in);

	/*
	 * This login method check username, password validity. you can enter correct
	 * username , password then you can access this system.
	 */
	public void login(String inusername, String inpassword) {
		int n = 1;
		do {

			System.out.println("\tLOGIN");
			System.out.println("username: ");
			username = scan.nextLine();

			System.out.println("password: ");
			password = scan.nextLine();

			if (inusername.equals(username) && inpassword.equals(password)) {
				System.out.println(" YOU ARE SUCCESSFULLY LOGIN ");
				break;

			} else if (inusername.equals(username)) {
				System.out.println(" Invalid Password ");
				n = 1;

			} else if (inpassword.equals(password)) {
				System.out.println(" Invalid Username ");
				n = 1;

			} else {
				System.out.println(" Invalid Username & Password ");
				System.out.println(" try again!!!");
				n = 1;
			}
		} while (n == 1);
	}

	public void student() {

		String inusername = "silaxan";
		String inpassword = "1234";
		Login demo = new Login();
		demo.login(inusername, inpassword);
		Student panel = new Student();
		panel.display();

	}

	public void teacher() {
		Login demo = new Login();

		String inusername = "sabashan";
		String inpassword = "1234";
		demo.login(inusername, inpassword);
		Teacher panel = new Teacher();
		panel.display();

	}

	public void choise() {
		Login demo = new Login();
		System.out.println();
		System.out.println("\t\t\tSCHOOL MENU PANEL");
		System.out.println("\t\t1. LOGIN\t2. CREATE ACCOUNT\t");
		System.out.println("Enter a choice: ");
		menuChoice = scan.nextInt();

		if (menuChoice == 1) {
			demo.teacher();
		} else {

		}
	}

	public static void main(String args[]) {
		Login demo = new Login();
		int n = 1;

		Scanner input = new Scanner(System.in);
		System.out.println("\t\tPLEASE TYPE  ");
		System.out.println("\t\t***********  ");

		do {
			System.out.println("             TEACHER OR STUDENT ");
			String usertype = input.nextLine();
			userinput = usertype.toUpperCase();

			if (userinput.equals("TEACHER")) {
				demo.teacher();
				break;
			} else if (userinput.equals("STUDENT")) {
				demo.student();
				break;

			} else {
				System.out.println();
				System.out.println("INVALID USER TYPE!!!");
				System.out.println(" !!TRY AGAIN !!");
				System.out.println();
				n = 1;
			}
		} while (n == 1);
	}
}
